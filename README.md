# Detailed Pipeline Overview for `.gitlab-ci.yml`

## Pipeline Description

This GitLab CI/CD pipeline is designed for building, securing, and deploying a PostgreSQL evaluation Docker image (`postgres-eval`) through various stages, with integration into Azure services.

## Pipeline Stages

### 1. Security Gates
- **Spectral Secret Gate:** Scans for secrets in the codebase using SpectralOps.
- **Spectral IaC Gate:** Scans Infrastructure as Code (IaC) for potential issues.

### 2. Build Image (`build_image`)
- Compiles the PostgreSQL evaluation Docker image named `postgres-eval`.
- Tags the image and pushes it to the GitLab registry.

### 3. ShiftLeft Security Scan (`shiftleft_scan`)
- Pulls the Docker image from the GitLab registry.
- Scans the image using ShiftLeft to identify vulnerabilities.

### 4. Push to Azure Container Registry (ACR) (`push_to_acr`)
- Pushes the Docker image from the GitLab registry to Azure Container Registry.

### 5. ACR ShiftLeft Scan (`acr_shiftleft_scan`)
- Scans the Docker image within Azure Container Registry using ShiftLeft for additional security assurance.

### 6. Deploy to Azure Kubernetes Service (AKS) (`deploy_to_aks`)
- Deploys the Docker image to Azure Kubernetes Service.
- Utilizes `kubectl` for applying Kubernetes deployment configurations.

## Variables

- `IMAGE_NAME`: "postgres-eval" - The name of the Docker image.
- `IMAGE_TAG`: Tag for the Docker image in GitLab's registry.
- `AZURE_IMAGE_TAG`: Tag for the Docker image in Azure Container Registry.

## Key Points

- Focus on Security: Emphasizes on securing the Docker image at various stages.
- Azure Integration: Seamlessly integrates with Azure Container Registry and Azure Kubernetes Service.
- Automated Workflow: Utilizes Docker and Kubernetes tools for a smooth workflow.

### Best Practices

- Ensure the `.gitlab-ci.yml` file is updated as the project evolves.
- Keep sensitive credentials secure.
- Regularly monitor and optimize the pipeline performance.
